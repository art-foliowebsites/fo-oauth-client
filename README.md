# oauth client
A curl base client helper to request for oauth api.

### Installation

#### update your composer.json
```php
    "repositories": [
        {
            "type":"vcs",
            "url":"https://bitbucket.org/photoup/fo-oauth-client.git"
        }
    ],

    "require": {
        "folio/oauth-client": "dev-master"
    }
```

### Usage

#### Initialize builder
```php
        use Folio\OauthClient\ResourceRequest;

        $fr = new ResourceRequest();
```

#### Upon request Token
```php
        use Folio\OauthClient\ResourceRequest;

        $data = $request->all();

		$requestData = [
                'grant_type'      => $data['grant_type'],
                'client_id'       => $data['client_id'],
                'client_secret'   => $data['client_secret'],
                'scope'           => 'auth.users'
            ];

        $fr = new ResourceRequest();
        $resToken = $fr->setUrl($url)
                       ->requestToken($requestData); // note AUTH_URL must set in .env

        $tokenize = json_decode($resToken, true);
```

#### Validate Token
```php
        use Folio\OauthClient\ResourceRequest;

		$fr = new ResourceRequest();
		$fr->setUrl(env($url)
           ->authenticate($tokenFilename); // validate token
```